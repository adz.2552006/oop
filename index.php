<?php
require_once('sheep.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");
echo "Name : " . $sheep->name . "<br>";
echo "Legs : " . $sheep->legs . "<br>";
echo "cold blooded : " . $sheep->cold_blooded . "<br>";
echo "<br>";
$frog = new frog("buduk");
echo "Name : " . $frog->name1 . "<br>";
echo "Legs : " . $frog->legs1 . "<br>";
echo "cold blooded : " . $frog->cold_blooded1 . "<br>";
echo "Jump : " . $frog->jump . "<br>";
echo "<br>";
$ape = new ape("suongokong");
echo "Name : " . $ape->name2 . "<br>";
echo "Legs : " . $ape->legs2 . "<br>";
echo "cold blooded : " . $ape->cold_blooded2 . "<br>";
echo "yell : " . $ape->yell . "<br>";

?>